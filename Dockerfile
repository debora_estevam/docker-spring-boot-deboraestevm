FROM openjdk:12
ADD target/docker-spring-boot-deboraestevm.jar docker-spring-boot-deboraestevm.jar
EXPOSE 8085
ENTRYPOINT ["java", "-jar", "docker-spring-boot-deboraestevm.jar"]
